function init() {
    angular.element(document).ready(function() {
        window.init();
    });
}

(function () {
    "use strict";

    // app
    var todoApp = angular.module('todoApp', ['ngRoute']);


    // config
    todoApp.config(function($routeProvider) {
        $routeProvider.when('/', { templateUrl: 'partials/todo-list.html', controller: 'todoController'})
            .when('/todo-detail/:detailId', { templateUrl: 'partials/todo-detail.html', controller: 'todoDetailController'})
            .when('/todo-create', { templateUrl: 'partials/todo-create.html', controller: 'todoCreateController'})
            .otherwise({ redirectTo: '/'});
    });

    // service for calling the Google CloudEndpoint backend
    todoApp.factory('CloudEndpoint', function ($q) {
            var initDone = false;
            var authorized = false;

            return {
                init: function() {
                        var ROOT = 'https://buoyant-sum-777.appspot.com/_ah/api';
                        var hwdefer=$q.defer();
                        var oauthloaddefer=$q.defer();
                        var oauthdefer=$q.defer();
                        var s = this;

                        gapi.client.load('todo', 'v1', function() {
                            hwdefer.resolve(gapi);
                            s.initDone = true;
                        }, ROOT);
                        gapi.client.load('oauth2', 'v2', function(){
                            oauthloaddefer.resolve(gapi);
                        });
                        var chain=$q.all([hwdefer.promise,oauthloaddefer.promise]);
                        return chain;
                },
                getInitDone: function() {
                    return this.initDone;
                },
                loadTodo: function(id) {
                    var p = $q.defer();
                    var requestData = {};
                    requestData.id = id;
                    var s = this;
                    var get = gapi.client.todo.todo.get(requestData);

                    get.execute(function(resp) {
                        if(resp) {
                            p.resolve(resp);
                        } else {
                            p.reject('Error fetching Todo ' + id);
                        }
                    });
                    return p.promise;
                },
                saveTodo: function(todo) {
                    var p = $q.defer();

                    gapi.client.todo.todo.save(todo).execute(function(resp) {
                        if (resp.id) {
                            p.resolve(resp.id);
                        } else {
                            p.reject("Cannot save Todo");
                        }
                    });
                    return p.promise;
                },
                loadTodos: function() {
                    var p = $q.defer();

                    gapi.client.todo.todo.list().execute(function(resp) {
                        if (resp) {
                            p.resolve(resp.items);
                        } else {
                            p.reject("Cannot load the Todos");
                        }
                    });
                    return p.promise;
                },
                signin: function(callback) {
                    if(!authorized) {
                        gapi.auth.authorize({client_id: '900231941381-qefp8nhvm54pv8lnk76a4i3le6f4sckb.apps.googleusercontent.com', scope: 'https://www.googleapis.com/auth/userinfo.email',
                            immediate: false}, callback);
                        authorized = true;
                    }

                },
                getGivenName: function() {
                    var p = $q.defer();

                    gapi.client.oauth2.userinfo.get().execute(function(resp) {
                        if (!resp.code) {
                            p.resolve(resp.given_name);
                        } else {
                            p.reject("error fetching username");
                        }
                    });

                    return p.promise;
                }
            };
        });

    // controller to show the todo detail (partial: todo-detail.html)
    todoApp.controller('todoDetailController', function ($scope, $routeParams, $window, CloudEndpoint, $timeout){

        // on refresh, this works
        $window.init = function() {
            console.log('init');
            $scope.$apply($scope.initgapi());
        };

        $scope.initgapi = function() {
            CloudEndpoint.init().then(function() {
                $scope.loadTodo();
            });
        };

        $scope.loadTodo = function() {
            var id = $routeParams.detailId;

            $timeout(function() {
                CloudEndpoint.loadTodo(id).then(function(resp){
                        $scope.todo = resp;
                        $scope.is_backend_ready = true;
                }, function(errorString) {
                    console.log(errorString);
                });
            }, 0);
        };

        if (CloudEndpoint.getInitDone()) {
            $scope.loadTodo();
        }
    });

    // controller to create a new todo item (partial: todo-create.html)
    todoApp.controller('todoCreateController', function ($scope, $window, CloudEndpoint) {
        $window.init= function() {
            $scope.$apply($scope.initgapi);
        };

        $scope.initgapi = function() {
            CloudEndpoint.init().then(function(resp) {
                $scope.is_backend_ready = true;
            }, function() {
                console.log('NOT inited');
            });
        };

        $scope.success=false;
        $scope.error=false;

        $scope.createNew = function(){
            var todo =  $scope.todo;
            todo.done = false;

            CloudEndpoint.saveTodo(todo).then(function(){
               $scope.success = true;
               $scope.clearForm();
            }, function() {
               $scope.error = true;
            });
        };
        $scope.clearForm = function(){
            $scope.todo = {};
        };
    });

    // controller to load the list of todos (partial: todo-list.html). This is the inital "page"
    todoApp.controller('todoController', function ($scope, $window, CloudEndpoint) {

        $window.init= function() {
            $scope.$apply($scope.initgapi);
        };

        $scope.initgapi = function() {
            console.log('in initgapi');
            CloudEndpoint.init().then(function(resp) {
                CloudEndpoint.signin(function() {
                    CloudEndpoint.getGivenName().then(function(name){
                        $scope.givenName = name;
                    });
                    $scope.list();
                });

            }, function() {
                console.log('NOT inited');
            });
        };

        $scope.list = function() {
            console.log('init done, loading todos');
            CloudEndpoint.loadTodos().then(function(resp) {
                $scope.todoItems = resp;
                $scope.is_backend_ready = true;
            }, function(errorMessage) {
                console.log(errorMessage);
            });
        };

        $scope.remaining = function() {
            var count = 0;
            angular.forEach($scope.todoItems, function(todo) {
                count += todo.done ? 0 : 1;
            });
            return count;
        };
    });

    // filter to "pretty print" text
    todoApp.filter('prettyPrint', function () {
        return function (text) {
            if(text) {
                return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
            }
        };
    });

    // directive for a todo item in the todo-list
    todoApp.directive('todo', function () {
        return {
            restrict: 'E',
            scope: {
                link: '@url',
                todoItem: '=item'
            },
            template: '<input type="checkbox" ng-model="todoItem.done"> <a href="{{ link }}"><span class="done-{{ todoItem.done }}">{{ todoItem.name | prettyPrint}}</span></a>'
        } ;
    });
}());
